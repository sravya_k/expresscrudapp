const Pool = require('pg').Pool
const pool = new Pool({
  user: 'postgres',
  host: 'localhost',
  database: 'insurance',
  password: 'postgres',
  port: 5432,
})

const getUsers = (request, response) => {
  pool.query('SELECT * FROM users ORDER BY policyNumber ASC', (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json(results.rows)
  })
}

const createUser = (request, response) => {
  const { policyNumber, policyStartDate, policyEndDate, policyDescriptions, customerFirstName, customerSurname, customerDateOfBirth } = request.body

  pool.query('INSERT INTO users (policyNumber, policyStartDate, policyEndDate, policyDescriptions, customerFirstName, customerSurname, customerDateOfBirth) VALUES ($1, $2, $3, $4, $5, $6, $7)', [policyNumber, policyStartDate, policyEndDate, policyDescriptions, customerFirstName, customerSurname, customerDateOfBirth], (error, results) => {
    if (error) {
      throw error
    }
    response.status(201).json({"message":`User added`})
  })
}

const updateUser = (request, response) => {
  
  const { policyNumber, policyStartDate, policyEndDate, policyDescriptions, customerFirstName, customerSurname, customerDateOfBirth } = request.body

  pool.query(
    'UPDATE users SET policyStartDate = $2, policyEndDate = $3, policyDescriptions = $4, customerFirstName = $5, customerSurname = $6, customerDateOfBirth = $7 WHERE policyNumber = $1',
    [policyNumber, policyStartDate, policyEndDate, policyDescriptions, customerFirstName, customerSurname, customerDateOfBirth],
    (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json({"message":`User modified with policyNumber: ${policyNumber}`})
    }
  )
}

const deleteUser = (request, response) => {
  
  const policyNumber = parseInt(request.params.policyNumber)

  pool.query('DELETE FROM users WHERE policyNumber = $1', [policyNumber], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).json({"message":`User deleted with policyNumber: ${policyNumber}`})
  })
}

module.exports = {
  getUsers,
  createUser,
  updateUser,
  deleteUser,
}
const express = require('express')
const app = express()
const db = require('./queries')
const bodyParser = require('body-parser')
var cors = require('cors')
const port = 3000

app.use(cors())
app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
)

app.get('/', (req, res) => {
  res.end('Hello World!');
});

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`)
});

app.get('/users', db.getUsers)
app.post('/users', db.createUser)
app.put('/users/:policyNumber', db.updateUser)
app.delete('/users/:policyNumber', db.deleteUser)